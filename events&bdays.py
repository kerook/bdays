#!/usr/bin/env python3

import pandas as pd
from icalendar import Calendar, Event, vDate #https://icalendar.readthedocs.io/en/latest/usage.html
import os

def newcal(df):
    cal = Calendar()
    cal.add('prodid', 'me')
    cal.add('version', '2.0')
    for i in range(len(df.index)):
        event = Event()
        event.add('summary', df['SUMMARY'].loc[i])
        event.add('dtstart', vDate.from_ical(str(df['DTSTART'].loc[i])))
        event.add('uid',df['SUMMARY'].loc[i]+"00"+str(i))
        if (df['LOCATION'].loc[i]!=""):
            event.add('location',df['LOCATION'].loc[i])
        cal.add_component(event)
    return(cal)

def newcalbday(df):
    cal = Calendar()
    cal.add('prodid', 'me')
    cal.add('version', '2.0')
    for i in range(len(df.index)):
        event = Event()
        event.add('summary', df['SUMMARY'].loc[i])
        event.add('dtstart', vDate.from_ical(str(df['DTSTART'].loc[i])))
        #event.add('dtend', vDate.from_ical(str(int(df['DTSTART'].loc[i])+1000000)))
        event.add('uid',"BDay00"+str(i))
        event.add('description', df['SUMMARY'].loc[i]+"'s BDay: "+str(vDate.from_ical(str(df['DTSTART'].loc[i]))))
        event.add('RRULE', {'FREQ':['YEARLY'],'INTERVAL':1,'COUNT':100})
        if ('LOCATION' in df.columns):
            if (df['LOCATION'].loc[i]!=""):
                event.add('location',df['LOCATION'].loc[i])
        cal.add_component(event)
    return(cal)

events = pd.read_csv('./events.csv', encoding='utf8')
cal_events=newcal(events)
f = open(os.path.join('./', 'events.ics'), 'wb')
f.write(cal_events.to_ical())
f.close()

bdays = pd.read_csv('./bdays.csv', encoding='utf8')
cal_bday=newcalbday(bdays)
f = open(os.path.join('./', 'bdays.ics'), 'wb')
f.write(cal_bday.to_ical())
f.close()
